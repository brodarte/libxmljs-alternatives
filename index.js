const { performance, PerformanceObserver } = require("perf_hooks");

const xpath = require("xpath");
const dom = require("xmldom").DOMParser
const libxmljs = require("libxmljs");
const fastXmlParser = require("fast-xml-parser");
const xml2js = require("xml2js").parseString;
const parseXml = require("@rgrove/parse-xml");
const xmldoc = require("xmldoc");
const xmljs = require("xml-js");
const pixlxml = require("pixl-xml");

// A comparison of various xml parsers/converters as possible replacements of libxmljs.

// example xml string from a call to Neptune's userproperties.aspx service
const testXMLString = '<User id="570428179" groupId="202240" accountId="102240" sessionId="302486782" authenticationMethod="SS_CANDY"><SessionKey>a2J3V3ZtZ0ppfGhsDeFucDjyTwGIga1xhNVFUQ~~</SessionKey><LoginName>DmpAdmin</LoginName><AccountName>BDESupportQA</AccountName><Account Expiration="11/30/2024" /><GroupName>Admin</GroupName><Folders><Folder Name="PublicTemp" ID="0" Property="$(PUBLIC_TEMP)" Permissions="RW" Type="DOFS" UriBase="http://internal.spatialstream.com/" Db="TEMP_DATABASE1" DofsRset="PublicTemp" /><Folder Name="PUBLIC_FOLDER" ID="460" Property="$(USERDATA_ROOT)/PUBLIC_FOLDER/" Permissions="R" Type="DOFS" UriRoot="http://internal.spatialstream.com/RSET_S03" /><Folder Name="ss/imagery/digitalglobe" ID="8599" AccountID="102240" Property="$(USERDATA_ROOT)/ss/imagery/digitalglobe/" Permissions="R" Type="DB" Db="CUSTOMERLAYERS_SQLSPATIAL" UriRoot="http://internal.spatialstream.com/RSET_D01/" /><Folder Name="SEARCH_RESULT" ID="574" Property="$(SEARCH_RESULT_ROOT)" Permissions="RW" Type="DOFS" /><Folder Name="public_temp" ID="0" Property="$(PUBLIC_TEMP)" Permissions="RW" Type="DOFS" UriBase="http://internal.spatialstream.com/" Db="TEMP_DATABASE1" DofsRset="public_temp" /><Folder Name="TEMP" ID="0" Property="C:\temp\local" Permissions="RW" Type="DOFS" UriBase="http://internal.spatialstream.com/" Db="TEMP_DATABASE1" DofsRset="TEMP" /><Folder Name="ss/prop/bde/parceldetail" ID="6888" AccountID="102240" Property="$(USERDATA_ROOT)/ss/prop/bde/parceldetailJT2/" Permissions="R" Type="DB" Db="CUSTOMERLAYERS_SQLSPATIAL" UriRoot="http://internal.spatialstream.com/RSET_D01/" /><Folder Name="ACCOUNT_FOLDER" ID="5539" AccountID="102240" Property="$(USERDATA_ROOT)/BDESupportQA/Dmp/" Permissions="R" Type="DB" Db="CUSTOMERLAYERS_SQLSPATIAL" UriRoot="http://internal.spatialstream.com/RSET_D01/" /><Folder Name="_T80" ID="0" Property="c:\temp\local\" Permissions="RW" Type="DOFS" UriBase="http://internal.spatialstream.com/" Db="TEMP_DATABASE1" DofsRset="_T80" /><Folder Name="group_folder" ID="0" GroupID="202240" Property="$(USERDATA_ROOT)/BDESupportQA\Groups\202240\group_folder" Permissions="R" Type="DB" Db="CUSTOMERLAYERS_SQLSPATIAL" UriRoot="http://internal.spatialstream.com/RSET_D01/" /><Folder Name="my_folder" ID="0" UserID="570428179" Property="$(USERDATA_ROOT)/BDESupportQA\Users\570428179\my_folder" Permissions="RW" Type="DB" Db="CUSTOMERLAYERS_SQLSPATIAL" UriRoot="http://internal.spatialstream.com/RSET_D01/" /></Folders><SKUs type="account"><LOGINSITE allow="true" /><BDE allow="true" /><TAXMAPS allow="true" /><BDE_WINTER_WHATSNEW allow="true" /><BDE_SPRING2016_WHATSNEW allow="true" /><BDE_SPRING16_2ND_RELEASE allow="true" /><AMAZONWORKFLOW allow="true" /><TIMEVIEW allow="true" /><SITEANALYSIS allow="true" /><CLQ allow="true" /><TERRAIN allow="true" /><OWNERPORTFOLIO allow="true" /></SKUs><SKUs type="group" /><SKUs type="user" /><Settings><Setting Name="CITY" ID="53270" AccountID="102240" Property="" /><Setting Name="STATE" ID="53271" AccountID="102240" Property="" /><Setting Name="ZIP" ID="53272" AccountID="102240" Property="" /><Setting Name="STARTPATH" ID="53273" AccountID="102240" Property="" /><Setting Name="AUTO_LOGIN_NETWORK_MASK_IP_DIGMAP_INTERNAL" ID="53274" AccountID="102240" Property="255.0.0.0" /><Setting Name="AUTO_LOGIN_NETWORK_NODE_IP_DIGMAP_INTERNAL" ID="53275" AccountID="102240" Property="10.0.0.0" /><Setting Name="AUTO_LOGIN_NETWORK_MASK_IP_DIGMAP_COX" ID="53276" AccountID="102240" Property="255.255.255.252" /><Setting Name="LOCK_CONCURR_USER" ID="53278" AccountID="102240" Property="true" /><Setting Name="GROUP_REQUIRED" ID="53279" AccountID="102240" Property="true" /><Setting Name="PRODUCT" ID="53280" AccountID="102240" Property="Builder" /><Setting Name="DATACENTERS" ID="53281" AccountID="102240" Property="http://dc1.spatialstream.com/" /><Setting Name="SUBSCRIPTIONSTATUS" ID="53282" AccountID="102240" Property="Trial" /><Setting Name="BDE" ID="53283" AccountID="102240" Property="b27" /><Setting Name="KNOWN_FOLDER_SHARE" ID="53284" AccountID="102240" Property="BDESupportQA.SHARE" /><Setting Name="KNOWN_FOLDER_PRODUCTION" ID="53285" AccountID="102240" Property="BDESupportQA.Public" /><Setting Name="PARCELS" ID="53286" AccountID="102240" Property="SS.Base.GEOCODEPARCELS/GEOCODEPARCELS" /><Setting Name="STREET_CENTERLINE" ID="53287" AccountID="102240" Property="SS.Base.TIGER/TIGER" /><Setting Name="KAT" ID="53288" AccountID="102240" Property="SS.Base.KAT/KAT" /><Setting Name="POSTAL_CENTROID" ID="53289" AccountID="102240" Property="SS.Base.ZIP9/ZIP9,SS.Base.ZIP7/ZIP7,SS.Base.ZIP5/ZIP5" /><Setting Name="ZIP9" ID="53290" AccountID="102240" Property="SS.Base.ZIP9/ZIP9" /><Setting Name="ZIP7" ID="53291" AccountID="102240" Property="SS.Base.ZIP7/ZIP7" /><Setting Name="ZIP5" ID="53292" AccountID="102240" Property="SS.Base.ZIP5/ZIP5" /><Setting Name="SUBMARKET_STATS" ID="53293" AccountID="102240" Property="TRUE" /><Setting Name="LVMOBILE" ID="53294" AccountID="102240" Property="true" /><Setting Name="SKU_TAXMAP" ID="53295" AccountID="102240" Property="true" /><Setting Name="MAXUSERS" ID="53296" AccountID="102240" Property="75" /><Setting Name="ACCOUNTLASTMODIFIED" ID="53297" AccountID="102240" Property="2018-04-09 18:36:52.000" /><Setting Name="ENABLE_DOCIMAGES" ID="73686" AccountID="102240" Property="true" /><Setting Name="PAYMENT_EXEMPT" ID="73687" AccountID="102240" Property="false" /><Setting Name="PREPAYFUNDS" ID="73688" AccountID="102240" Property="false" /><Setting Name="AUTO_LOGIN_NETWORK_NODE_IP_DIGMAP_ATT" ID="78124" AccountID="102240" Property="23.120.235.221" /><Setting Name="AUTO_LOGIN_NETWORK_MASK_IP_DIGMAP_ATT" ID="78125" AccountID="102240" Property="255.255.255.255" /><Setting Name="AUTO_LOGIN_NETWORK_NODE_IP_DIGMAP_COX" ID="79823" AccountID="102240" Property="70.168.248.186 " /><Setting Name="CROSSLAYERQUERYLIMIT" ID="83689" AccountID="102240" Property="10000" /><Setting Name="ARCGISINTEGRATION" ID="89951" AccountID="102240" Property="true" /><Setting Name="CLQ" ID="96159" AccountID="102240" Property="true" /><Setting Name="heat_map_allowed" ID="97722" AccountID="102240" Property="true" /><Setting Name="DefaultDataServer" ID="105292" AccountID="102240" Property="JupiterTier2" /><Setting Name="MaxRecords" ID="105602" AccountID="102240" Property="20000" /><Setting Name="MaxRecords.GEOMETRY" ID="105603" AccountID="102240" Property="20000" /><Setting Name="OWNERPORTFOLIO" ID="110188" AccountID="102240" Property="BDESupportQA/OWNERPORTFOLIO" /><Setting Name="KNOWN_FOLDER_GROUP_FOLDER" ID="4402" GroupID="202240" Property="BDESupportQA.Public.Admin" /></Settings></User>'

// GitHub: https://github.com/jindw/xmldom
// GitHub: https://github.com/goto100/xpath#readme
// NPM: https://www.npmjs.com/package/xmldom
// NPM: https://www.npmjs.com/package/xpath
function testXPathXDOM() {
  // combination of xmldom and xpath
  // maybe good chance for backwards compatibility (with a shim) because the xpath syntax is also used in libxmljs
  const doc = new dom().parseFromString(testXMLString);
  const dataCenters = (xpath.select1("//Setting[@Name='DATACENTERS']/@Property", doc)).value;
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - xpath + xdom");
  }
}

// GitHub: https://github.com/libxmljs/libxmljs#readme
// NPM: https://www.npmjs.com/package/libxmljs
function testLibXMLJS() {
  // libxmljs
  const doc = libxmljs.parseXmlString(testXMLString);
  const dataCenters = doc.get("//Setting[@Name='DATACENTERS']").attr("Property").value();
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - libxmljs");
  }
}

// GitHub: https://github.com/NaturalIntelligence/fast-xml-parser#readme
// NPM: https://www.npmjs.com/package/fast-xml-parser
function testFastXMLParser() {
  const doc = fastXmlParser.parse(testXMLString, { ignoreAttributes: false, attributeNamePrefix: "", parseAttributeValue: true });
  // filter and find have similar performance here, filter can return many results if that's what is needed
  // const dataCenters = doc.User.Settings.Setting.filter(setting => setting.Name === "DATACENTERS")[0].Property;
  const dataCenters = doc.User.Settings.Setting.find(setting => setting.Name === "DATACENTERS").Property;
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - fast-xml-parser");
  }
}

// GitHub: https://github.com/Leonidas-from-XIV/node-xml2js
// NPM: https://www.npmjs.com/package/xml2js
function testXML2JS() {
  xml2js(testXMLString, (err, res) => {
    // xml2js adds '$' as the key to the attribute objects
    const dataCenters = (res.User.Settings[0].Setting.find(setting => setting.$.Name === "DATACENTERS").$.Property);
    if (dataCenters !== "http://dc1.spatialstream.com/") {
      console.log("ERROR - xml2js");
    }
  });
}

// GitHub: https://github.com/rgrove/parse-xml
// NPM: https://www.npmjs.com/package/@rgrove/parse-xml
function testParseXML() {
  const doc = parseXml(testXMLString)
  // the resulting object format makes it hard to access properties...need to hard-code exact indices or use multiple find/filters
  const dataCenters = doc.children[0].children[9].children.find(setting => setting.attributes.Name === "DATACENTERS").attributes.Property;
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - parse-xml");
  }
}

// GitHub: https://github.com/nfarina/xmldoc#readme
// NPM: https://www.npmjs.com/package/xmldoc
function testXMLDoc() {
  const doc = new xmldoc.XmlDocument(testXMLString);
  const dataCenters = doc.childNamed("Settings").childWithAttribute("Name", "DATACENTERS").attr.Property;
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - xmlDoc");
  }
}

// GitHub: https://github.com/nashwaan/xml-js#readme
// NPM: https://www.npmjs.com/package/xml-js
function testXMLJS() {
  // JS/JSON output can be converted back to XML
  const doc = xmljs.xml2js(testXMLString, { compact: true });
  const dataCenters = doc.User.Settings.Setting.find(setting => setting._attributes.Name === "DATACENTERS")._attributes.Property;
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - xml-js");
  }
}

// GitHub: https://github.com/jhuckaby/pixl-xml
// NPM: https://www.npmjs.com/package/pixl-xml
function testPixlXML() {
  const doc = pixlxml.parse(testXMLString);
  const dataCenters = doc.Settings.Setting.find(setting => setting.Name === "DATACENTERS").Property;
  if (dataCenters !== "http://dc1.spatialstream.com/") {
    console.log("ERROR - pixl-xml");
  }
}

const wrappedXPathXDOM = performance.timerify(testXPathXDOM);
const wrappedLibXMLJS = performance.timerify(testLibXMLJS);
const wrappedFastXMLParser = performance.timerify(testFastXMLParser);
const wrappedXML2JS = performance.timerify(testXML2JS);
const wrappedParseXML = performance.timerify(testParseXML);
const wrappedXMLDoc = performance.timerify(testXMLDoc);
const wrappedXMLJS = performance.timerify(testXMLJS);
const wrappedTestPixlXML = performance.timerify(testPixlXML);

const obs1 = new PerformanceObserver((list) => {
  const latestResult = list.getEntries()[list.getEntries().length - 1];
  console.log(`${latestResult.name} finished in: ${latestResult.duration} ms`);
});

obs1.observe({ entryTypes: ['function'] });

console.log("\nTesting XML Parsers on a userproperties XML example: Parse XML file and get value of Property attribute for Setting element with Name attribute set to 'DATACENTERS'\n")


wrappedLibXMLJS();
wrappedXPathXDOM();
wrappedFastXMLParser();
wrappedXML2JS();
wrappedParseXML();
wrappedXMLDoc();
wrappedXMLJS();
wrappedTestPixlXML();
