# libxmljs-alternatives

### Description:

A collection of xml parsers/converters with performance comparisons against libxmljs

### Installation:

1. clone
2. npm install
3. node index.js

### Results so far...

Test task is to parse XML returned by Neptune's userproperties.aspx service and get the value of the Property attribute  
in the Setting element with the Name attribute set to DATACENTERS

testLibXMLJS finished in: 1.020359 ms  
testXPathXDOM finished in: 17.614279 ms  
testFastXMLParser finished in: 3.535503 ms  
testXML2JS finished in: 15.873178 ms  
testParseXML finished in: 7.674878 ms  
testXMLDoc finished in: 7.983105 ms  
testXMLJS finished in: 12.451564 ms  
testPixlXML finished in: 2.097332 ms  